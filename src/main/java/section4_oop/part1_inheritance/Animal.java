/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {

    public String name;
    public String movementType;
    public double speed;
    public double maxSpeed;
    public int age;
    public int maxAge;



    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setAge(int age) {
        this.age = age;
    }

    /**
     * returns the name of the animal
     * @return name the species name
     */
    public String getName() {
        return null;
    }
    
    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return null;
    }
    
    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public double getSpeed(double maxSpeed, int maxAge, int age) {

        return this.speed = maxSpeed * (0.5 + (0.5 * ((maxAge - age) / maxAge)));
    }

    public int getAge(){
        return 0;
    }
    
}
