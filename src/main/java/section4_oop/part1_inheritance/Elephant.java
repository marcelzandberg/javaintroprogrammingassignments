/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Elephant extends Animal {

    private static final double maxSpeed = 40;
    private static final int maxAge = 86;
    private static final String movingType = "thunder";

    private int age;

    public Elephant(int age) {
        this.age = age;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public double getSpeed(double maxSpeed, int maxAge, int age) {
        return super.getSpeed(maxSpeed, maxAge, age);
    }

    @Override
    public String toString() {
        return "Elephant{" +
                "speed=" + speed +
                '}';
    }
}
