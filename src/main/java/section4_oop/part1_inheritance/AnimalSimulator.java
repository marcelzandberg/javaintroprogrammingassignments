/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

import java.util.List;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class AnimalSimulator {
    public static void main(String[] args) {
        AnimalSimulator anSim = new AnimalSimulator();
        anSim.start(args);
    }

    
    private void start(String[] args) {
        //start processing command line arguments
        int argNumber = 0;
        for( String arg : args){
            System.out.println(arg);
            switch (arg){
                case "Elephant":

                    //System.out.println(Integer.parseInt(args[argNumber + 1]));
                    Elephant elephant = new Elephant(Integer.parseInt(args[argNumber + 1]));
                    break;
                case "Horse":
                    Horse horse;
                    break;
                case "HouseMouse":
                    HouseMouse houseMouse;
                    break;
                case "Tortoise":
                    Tortoise tortoise;
                    break;
                default:
                    System.out.println("use these animals:");
            }
            argNumber++;
        }
    }
    
    /**
     * returns all supported animals as List, alhabetically ordered
     * @return supportedAnimals the supported animals
     */
    public List<String> getSupportedAnimals() {
        return null;
    }
}
