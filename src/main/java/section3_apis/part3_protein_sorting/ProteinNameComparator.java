package section3_apis.part3_protein_sorting;

import java.util.Comparator;

public class ProteinNameComparator implements Comparator<Protein> {
    @Override
    public int compare(Protein protein, Protein t1) {
        return protein.getName().compareTo(t1.getName());
    }
}
