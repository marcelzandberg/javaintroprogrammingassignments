package section3_apis.part3_protein_sorting;

import java.util.Comparator;

public class ProteinAccessionNumberComparator implements Comparator<Protein> {
    @Override
    public int compare(Protein protein, Protein t1) {
        return protein.getAccession().toUpperCase().compareTo(t1.getAccession().toUpperCase());
    }
}
