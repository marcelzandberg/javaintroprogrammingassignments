package section1_intro.part1_language_basics;

public class Square {
    Point upperLeft;
    Point lowerRight;

    private int x1;
    private int x2;
    private int y1;
    private int y2;


    public Square(Point upperLeft, Point lowerRight, int x1, int x2, int y1, int y2) {
        this.upperLeft = upperLeft;
        this.lowerRight = lowerRight;
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }

    /**
     * returns the surface defined by the rectangle with the given upper left and lower right corners.
     * It assumes two corners have been created already!
     * @return
     */


    int surface(){
        //calculate surface - can you implement this?
        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);

        Long square = Math.round(upperLeft.euclideanDistanceTo(p1,p2) + lowerRight.euclideanDistanceTo(p1, p2));

        return square.intValue();
    }


}