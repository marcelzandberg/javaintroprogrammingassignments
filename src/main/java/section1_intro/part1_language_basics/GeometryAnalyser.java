package section1_intro.part1_language_basics;

public class GeometryAnalyser {


    public static void main(String[] args) {
        //your code here

        for (String arg : args) {
            System.out.println("arg = " + arg);
        }

        int x1 = Integer.parseInt(args[0]);
        int y1 = Integer.parseInt(args[1]);
        int x2 = Integer.parseInt(args[2]);
        int y2 = Integer.parseInt(args[3]);
        String desiredOperation = args[4];

        if(desiredOperation.equals("dist")){
            Point p1 = new Point(x1, y1);
            Point p2 = new Point(x2, y2);
            System.out.println("distance = " + Math.round(p1.euclideanDistanceTo(p1, p2)));
        }

        if(desiredOperation.equals("surf")){
            if(x1 < x2 || y1 < y2) throw new IllegalArgumentException();

            Point p1 = new Point(x1, y1);
            Point p2 = new Point(x2, y2);
            Square s = new Square(p1, p2, x1, x2, y1, y2);
            System.out.println("surface = " +s.surface());
        }

    }
}