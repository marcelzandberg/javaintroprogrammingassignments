package section1_intro.part1_language_basics;


public class Point {
    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    double euclideanDistanceTo(Point Point1, Point Point2) {
        //calculate distance - can you implement this?


        double euclidianDistance = Math.sqrt((Point2.y - Point1.y) * (Point2.y - Point1.y) + (Point2.x - Point1.x) * (Point2.x - Point1.x));
        return euclidianDistance;
    }

    public static void main(String[] args) {
        Point p1 = new Point(1,2);
        Point p2 = new Point(3,4);
        p1.euclideanDistanceTo(p1, p2);
    }
}