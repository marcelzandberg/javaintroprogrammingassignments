package section2_syntax.part1_datatypes;

/**
 * Creation date: May 28, 2018
 *
 * @author Michiel Noback (&copy; 2018)
 * @version 0.01
 */
public class Datatypes {
    /*
    * For the following couple of methods, simply return the name of the datatype that best suits
    * the given use case, as efficiently as possible.
    * Choose from
    *   - boolean
    *   - char
    *   - byte
    *   - short
    *   - int
    *   - long
    *   - double
    *   - float
    *   - String
    *
    * The first one has already been implemented as example.
    */


    String correctDataType0() {
        //USE CASE / USAGE:
        //Number of days within a year
        return "short";
    }

    String correctDataType1() {
        //USE CASE / USAGE:
        //Age of the universe in whole years
        return "long";
    }

    String correctDataType2() {
        //USE CASE / USAGE:
        //Turnover of ATP in a cell, in Molar per minute
        return "float";
    }

    String correctDataType3() {
        //USE CASE / USAGE:
        //Molecular weight of a protein, in Daltons
        return "float";
    }

    String correctDataType4() {
        //USE CASE / USAGE
        // the live/death status of a test animal
        return "boolean";
    }

    String correctDataType5() {
        //USE CASE / USAGE
        // the name of an app user
        return "String";
    }
    String correctDataType6() {
        //USE CASE / USAGE
        // encoding of human gender (Male, Female, Undefined)
        return "char";
    }

    /**
     * The method below is supposed to calculate the G+C fraction of a DNA sequence.
     * It should do so case insensitive.
     * After uncommenting the method body (select the blok and press Ctrl + /), there are 3 problems and one error
     * in this method you should find and fix before the tests will pass.
     */
    double determineGCfraction(String theDNA) {
        theDNA = theDNA.toUpperCase();
        int gcCount = 0;// not initilized
        for (int i = 0; i < theDNA.length(); i++) {
            char nucleotide = theDNA.charAt(i);

            if (nucleotide == 'C' || nucleotide == 'G') {
                gcCount++;
            }
        }
        double fraction = (double)gcCount / theDNA.length();
        return fraction;
    }

    /**
     * The method below creates a string and calls on another method to change it.
     * Something goes wrong - can you fix it?
     */
    String modifyString() {
        String input = "Waar kan ik de koffiemachine vinden in dit gebouw?";
        input = replaceWord(input, "koffie", "soda");
        return input;
    }

    String replaceWord(String input, String replace, String replaceWith) {
        return input.replace(replace, replaceWith);
    }

    /**
     * The method below should return a String array of length 2, with the first and last element
     * of the input array in it, converted to String representations.
     * Hint: look at the methods available to class Object (GIYF)
     */
    String[] getFirstAndLast(Object[] input) {
        String[] elements = new String[2];
        elements[0] = input[0].toString();
        elements[1] = input[input.length-1].toString();
        return elements;
    }


    /**
     * This method should return a new int array where all integers from the input have been cubed (power of 3).
     * For convenience, the skeleton of a for loop is already provided.
     * @param input
     * @return cubedInput
     */
    int[] cubeAll(int[] input) {
        int arrayLength = input.length; //get the correct value
        int[] cubed = new int[arrayLength];
        for(int i = 0; i < arrayLength; i++) {
            cubed[i] = (int)Math.pow(input[i], 3);
        }
        return cubed;
    }

    /**
     * This method should return the cumulative product of all numbers in the input array.
     * @param input
     * @return cumProd the cumulative product
     */
    int cumulativeProduct(int[] input) {
        int toReturn = 0;
        for(int i : input){
            toReturn += i;
        }
        return toReturn;
    }

    public static void main(String[] args) {
        Datatypes d = new Datatypes();
        System.out.println(d.determineGCfraction("GALDAJHCCCCG"));
        System.out.println(d.modifyString());
        int[] input = {1,2,3,4};
        System.out.println(d.cumulativeProduct(input));
    }

}
