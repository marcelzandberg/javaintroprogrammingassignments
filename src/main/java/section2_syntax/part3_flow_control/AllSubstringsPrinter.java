/*
 * Copyright (c) 2014 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package section2_syntax.part3_flow_control;

/**
 *
 * @author michiel
 */
public class AllSubstringsPrinter {
    /**
     * main method serves development purposes only.
     * @param args the args
     */
    public static void main(final String[] args) {
        AllSubstringsPrinter asp = new AllSubstringsPrinter();
        asp.printAllSubstrings("GATCG", true, false); //should print left truncated, left aligned
    }

    /**
     * Prints all possible substrings according to arguments.
     * @param stringToSubstring the string to substring
     * @param leftTruncated flag to indicate whether the substrings should be truncated from the left (or the right)
     * @param leftAligned flag to indicate whether the substrings should be printed left-aligned (or right-aligned)
     */
    public void printAllSubstrings(String stringToSubstring, boolean leftTruncated, boolean leftAligned){
//        System.out.println(stringToSubstring.substring(0,4));
        if(leftTruncated & leftAligned){
            for (int i = 0; i < stringToSubstring.length(); i++){
                System.out.println(stringToSubstring.substring(i));

            }
        }else if((leftTruncated) & (!leftAligned)){
            String blank = "";
            for (int i = 0; i < stringToSubstring.length(); i++){
                System.out.println(blank + stringToSubstring.substring(i));
                blank += " ";
            }
        }else {
            for (int i = 0; i < stringToSubstring.length(); i++)
                System.out.println(stringToSubstring.substring(0, stringToSubstring.length() - i));
            }
        }
    }

