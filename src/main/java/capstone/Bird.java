package capstone;

public class Bird {


    private String id;
    private String name;
    private String scientificName;
    private int rarity;
    private String birdContent;

    public Bird(String id, String name, String scientificName, int rarity, String birdContent) {
        this.id = id;
        this.name = name;
        this.scientificName = scientificName;
        this.rarity = rarity;
        this.birdContent = birdContent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScientificName() {
        return scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public int getRarity() {
        return rarity;
    }

    public void setRarity(int rarity) {
        this.rarity = rarity;
    }

    public String getBirdContent() {
        return birdContent;
    }

    public void setBirdContent(String birdContent) {
        this.birdContent = birdContent;
    }

    @Override
    public String toString() {
        return "Bird{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", scientificName='" + scientificName + '\'' +
                ", rarity=" + rarity +
                ", birdContent='" + birdContent + '\'' +
                '}';
    }
}
