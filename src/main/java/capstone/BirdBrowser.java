package capstone;


import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BirdBrowser {

    private List<Bird> parseBirds() {
        String fileName = "/homes/mazandberg/bitbucket/introjava/javaintroprogrammingassignments/data/birds_observado_org.csv";
       // "C:\\Users\\marcel\\Documents\\studie\\thema9\\javaintroprogrammingassignments\\data\\birds_observado_org.csv";
        Path path = Paths.get(fileName);

        List<Bird> birds = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(path)) {
            String line;
            int lineNumber = 0;

            while ((line = br.readLine()) != null) {

                lineNumber++;
                //strips double quotes from lines
                line = line.replace("\"", "").replace(",", " ");

                if (lineNumber == 1) continue;
                String[] elements = line.split(";");

                //parse the elements
                String id = elements[0];
                String name = elements[1];
                String scientificName = elements[2];
                int rarity = Integer.parseInt(elements[4]);
                String birdContent = elements[5] + elements[6] + elements[7]+ elements[8]+ elements[9]+ elements[10]+ elements[11]+ elements[12]+ elements[13]+ elements[14]+ elements[15]+ elements[16];

                Bird bird = new Bird(id, name, scientificName, rarity, birdContent);
                birds.add(bird);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return birds;
    }

    private List<Bird> filterBirds(List<Bird> birds, String regex){

        List<Bird> filteredBirds = new ArrayList<>();


        Pattern pattern = Pattern.compile(regex);

        for (int i = 0; i < birds.toArray().length -1; i++){
            //find bird by regex
            Matcher matcher = pattern.matcher(birds.get(i).toString());
            if (matcher.find()){
                String match = matcher.group();
                System.out.println(match);
                if(birds.get(i).toString().contains(match)){

                    System.out.println(birds.get(i));
//                    Bird filtered = new Bird(birds.get(i).getId(), birds.get(i).getName(), birds.get(i).getScientificName(), birds.get(i).getRarity(), birds.get(i).getBirdContent());
                    filteredBirds.add(birds.get(i));
                }
            }
        }
        return filteredBirds;
    }

    private void start(String[] args){
        try{
            System.out.println("searching by pattern = " + args[0]);
            filterBirds(parseBirds(), args[0]);
        }catch (ArrayIndexOutOfBoundsException e){
            Logger.getLogger("BirdBrowser").log(Level.WARNING, "You need to give an filter option like '283749' for filtering on id");
        }
    }


    public static void main(String[] args) {
        BirdBrowser birdBrowser = new BirdBrowser();
        birdBrowser.start(args);

    }
}
